
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './Auth/Login/Login';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import Layout from './Modules/Layout';
import ErrorPages from './Error/ErrorPages';
import HomePage from './Modules/Pages/Home/HomePage';
import About from './Modules/Pages/About/About';
import Contact from './Modules/Pages//Contact/Contact';
import { useAuth } from './Auth/components/Auth';
import Product from './Modules/Pages/Product/Components/Product';
import ProductDetail from './Modules/Pages/Product/Components/ProductDetail';


function App() {
const {auth} = useAuth();
  return (
    <div>
    
      <BrowserRouter>
        <Routes>
          {auth ? <Route path="/" element={<Layout />}>
            <Route index element={<HomePage/>} />
            <Route path="about" element={<About/>} />
            <Route path="product" element ={<Product/>}/>
            <Route path="contact" element={<Contact/>}/>
            <Route path="product/:id" element ={<ProductDetail/>}/>
            <Route path="*" element={<Navigate to="/"/>}></Route>
          </Route>
          :
            <>
              <Route index path="auth/login" element={<Login/>} />
              <Route path="*" element={<Navigate to="/auth/login"/>}></Route>
            </>
          }
          <Route path="error" element={<ErrorPages/>}/>
          </Routes>
      </BrowserRouter>
    </div>
  );
}
export default App;
