import axios from "axios";
import {getAuth} from "../components/AuthHalper";

const login = (username,password) => {
   return  axios.post("https://dummyjson.com/auth/login", {
        username: username,
        password: password,
    })
}
const getUser = () => {
  return  axios.get("https://dummyjson.com/auth/me", {
        headers: {
            "Authorization": ` Bearer ${getAuth()} ` 
        }
    });
}
export {login,getUser}