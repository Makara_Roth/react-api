

import { useEffect } from 'react'
import { useAuth } from './Auth'
import { getUser } from '../Core/Requset';

const AuthInit = ({children}) => {
    const {auth, setUser , logout } = useAuth(); 

    useEffect(() => {
      const getCurrentUser = () => {
          getUser().then((reaponse) => {
              setUser(reaponse.data)
          }).catch(() => {
              logout();
          })
      }
      getCurrentUser();

      // eslint-disable-next-line
  }, [auth]);

    return children;
}

export { AuthInit } 