

import React from 'react'
import { Outlet } from 'react-router-dom';

const AuthPage = (params) => {
  return (
    <>
        <Outlet/>
    </>
  )
}

export default AuthPage