import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import { useAuth } from '../../../Auth/components/Auth';

const Headers = () => {
  const { logout } = useAuth();

  return (
    <>
      <Navbar bg="dark" data-bs-theme="dark" className=' position-sticky top-0 z-3'>
        <Container>
          <Navbar.Brand as={Link} to="/">Shop </Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll>
              <Nav.Link as={Link} to="/" > Home </Nav.Link>
              <Nav.Link as={Link} to="/product" > Product </Nav.Link>
              <Nav.Link as={Link} to="/about" > About </Nav.Link>
              <Nav.Link as={Link} to="/contact" > Contact </Nav.Link>
            </Nav>
            <button className=' btn btn-primary ms-5' onClick={() => {
              logout();
            }}>
              Logout
            </button>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default Headers