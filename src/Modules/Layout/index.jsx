import { Outlet } from 'react-router-dom'
import Headers from './Header/Header'


import React from 'react'

const Layout = () => {
  return (
    <>
        <Headers/>
        <Outlet/>
    </>
  )
}

export default Layout