
import React, { useEffect, useState } from 'react'
import { getCategories, getlistProduct, getListProductByCategory, searchProduct } from '../core/Requset'
import Card from 'react-bootstrap/Card';
import { useNavigate } from 'react-router-dom';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Badge } from "react-bootstrap";
import PaginationBar from "./Paginetion";



const ListProduct = () => {
    const [products, setProduct] = useState([])
    const navigate = useNavigate();
    const [searchText, setSearchText] = useState("");
    const [categories, setCategories] = useState([]);
    const [active, setActive] = useState("All");
    const [total, setTotal] = useState(0);

    const limit = 8;

    useEffect(() => {
        getlistProduct(limit).then((res) => {
            setProduct(res.data.products);
            setTotal(res.data.total);
        })
        getCategories().then((res) => {
            setCategories(res.data)
        })
    }, [])

    useEffect(() => {
        active === "All" ?
            getlistProduct(limit).then((res) => {
                setProduct(res.data.products);
                setTotal(res.data.total);


            })
            : getListProductByCategory(limit, active).then((res) => {
                setProduct(res.data.products);
                setTotal(res.data.total);

            });
    }, [active]);
    const onSearch = (e) => {
        searchProduct({ q: searchText }).then((res) => {
            setProduct(res.data.products)
        })
    }
    console.log(products)
    return (
        <>
            <div className={'d-flex justify-content-center mb-5 mt-5'}>
                <Form className="d-flex">
                    <Form.Control
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                        onChange={(e) => setSearchText(e.target.value)}
                    />
                    <Button onClick={onSearch} variant="outline-success">Search</Button>
                </Form>
            </div>
            <div className="px-1">
                <div className={' flex-wrap mt-3 py-1 pb-3'}>
                    <Badge onClick={() => setActive("All")} className="px-2 py-3 my-3 fs-6 mx-2  "
                        bg={active === "All" ? "primary" : "secondary"}>All</Badge>
                    {categories.map((item, index) =>
                        <Badge onClick={() => setActive(item)} key={index} className="px-2 py-3 fs-6 mx-2"
                            bg={active === item ? "primary" : "secondary"}>{item}</Badge>)}
                </div>
            </div>
            <div className='d-flex flex-wrap justify-content-center'>
                {products.length === 0 && <div className={'text-center'}> Not Found </div>}
                {products.map((product, index) => {
                    return (
                        <div onClick={() => navigate(`/product/${product.id}`)} className=' mx-2' key={index}>
                            <Card style={{ marginTop: "10px", overflow: "scroll", width: '18rem', height: "25rem" }}>
                                <Card.Img variant="top" style={{ width: "100%", height: " 50% " }}
                                    src={product.thumbnail} alt='...' />
                                <Card.Body className=' m-2'>
                                    <Card.Title>Title : {product.title}</Card.Title>
                                    <Card.Title>Price : {product.price}$</Card.Title>
                                    <Card.Title>Stock : {product.stock}</Card.Title>
                                    <Card.Text>category : {product.category}</Card.Text>
                                </Card.Body>
                            </Card>

                        </div>
                    )})
                }
            </div>
            <div className="d-flex justify-content-center my-3">
                <PaginationBar onChangePage={(number) => {
                    getlistProduct(limit , (number - 1) * limit ).then((res) => {
                        setProduct(res.data.products);
                        setTotal(res.data.total);
                    })
                }} total={total / limit} />
            </div>
        </>
    )
}

export default ListProduct