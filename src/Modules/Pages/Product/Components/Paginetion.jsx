import Pagination from 'react-bootstrap/Pagination';
import {useState} from "react";

const PaginationBar = ({ total, onChangePage }) => {
    const [active, setActive] = useState(1);

    let items = [];

    for (let number = 1; number <= total; number++) {

        items.push(
            <Pagination.Item onClick={() => {
                setActive(number);
                onChangePage && onChangePage(number); }}
                key={number}  active={number === active}>
                {number}
            </Pagination.Item>
        );
       
    } 
    return (
        <div>
            <Pagination size="lg">{items}</Pagination>
        </div>
    )
}
export default PaginationBar;

