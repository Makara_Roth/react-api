

import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { getProductById } from '../core/Requset';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules';
import "../Product.css"

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';




const ProductDetail = () => {
    const {id} = useParams();
    const [product, setProduct] = useState({});

    useEffect(() => {
        getProductById(id).then((res) => {
            setProduct(res.data)
        })
    }, [id])
    if (!product.id) {
        return <div className=" d-flex justify-content-center align-items-center fs-3">
            Loading...
        </div>
    }
    const pagination = {
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (index + 1) + '</span>';
        },
    };
    return (
        <div className="container ">
            <div className="row mx-auto mt-5 p-5">
                <div className={'col-md-6'} >
                    <Swiper
                        pagination={pagination}
                        modules={[Pagination]}
                        className="mySwiper"
                    >
                        <SwiperSlide> <img src={product.images[0]} alt={'..'}/> </SwiperSlide>
                        <SwiperSlide> <img src={product.images[1]} alt={'..'}/> </SwiperSlide>
                        <SwiperSlide> <img src={product.images[2]} alt={'..'}/> </SwiperSlide>
                        <SwiperSlide> <img src={product.images[3]} alt={'..'}/> </SwiperSlide>
                        <SwiperSlide> <img src={product.images[4]} alt={'..'}/> </SwiperSlide>
                    </Swiper>
                </div>
                <div className={'col-md-6 align-text-bottom'}>
                    <h2 > {product.title}</h2>
                    <h3> Price : {product.price}$</h3>
                    <h4> Rating : {product.rating}* </h4>
                    <h5> Stock : {product.stock}</h5>
                    <p> Category : {product.category}</p>
                    <p>{product.description}</p>
                </div>
            </div>
        </div>
    )
}

export default ProductDetail