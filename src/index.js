import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { AuthProviser } from "./Auth/components/Auth";
import { AuthInit } from "./Auth/components/AuthInit";
import {getUpAxios} from "./Auth/components/AuthHalper";


const root = ReactDOM.createRoot(document.getElementById("root"));
getUpAxios();
root.render(
  <React.StrictMode>
    <AuthProviser>
      <AuthInit>
        <App />
      </AuthInit>
    </AuthProviser>
  </React.StrictMode>
);
